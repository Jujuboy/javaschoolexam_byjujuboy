package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int i1 = 0; // Counter for the amount of matches
        int i2 = 0; // Iterator
/*
Next part counts how much times the variable from the 1st sequence
is equals the variable from the second
 */
        while (i2 < y.size()) {
            if ((i1 < x.size()) && (x.get(i1)).equals(y.get(i2))) {
                i1++;
            }
            i2++;
        }

        return i1 == x.size();
    }
}
