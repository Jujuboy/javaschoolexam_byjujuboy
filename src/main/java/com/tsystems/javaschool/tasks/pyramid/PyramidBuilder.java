package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;
import java.util.Collections;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) {

        int baseLenght = inputNumbers.size();
        int strAmount = (int)((-1+Math.sqrt(1+8*baseLenght))/2); //number of strings
        int colAmount = 2*strAmount-1; //number of columns

        Collections.sort(inputNumbers); //sorting input

        int [][] piramid = new int [strAmount][colAmount];
        int inputCounter = 0;//counter for sorted input

        //loop for array filling
        for(int i = 0; i < strAmount; i++){
            for(int k = 0; k < colAmount; k++){
                piramid[i][k] = 0;
            }

            int numberOfFills = i+1;
            int position = colAmount/2 - i;

            while (numberOfFills > 0){
                piramid[i][position] = inputNumbers.get(inputCounter);
                inputCounter++;
                numberOfFills--;
                position+=2;
            }
        }

        return piramid;
    }


}
